package com.example.laboratorio01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_Procesar.setOnClickListener {

            if (
                !edt_edad.text.toString().isNullOrEmpty()
            ){
                val edad = edt_edad.text.toString().toInt()

                if (edad >= 18)
                    txt_Resultado.text = "Usted es mayor de edad"
                else
                    txt_Resultado.text = "Usted es un menor de edad"
            }
            else{
                Toast.makeText(this,"La edad no puede estar vacia",Toast.LENGTH_SHORT).show()
            }
        }
    }
}